#!/bin/sh

make clean
./configure --enable-shared --disable-static \
 --shlibdir=@executable_path/../Frameworks \
 --disable-doc \
 --disable-decoders --disable-encoders --disable-devices --disable-muxers --disable-demuxers --disable-protocols \
 --enable-avutil --enable-avformat --enable-avcodec \
 --enable-swscale --enable-swresample \
 --enable-encoder=aac \
 --enable-bsf=h264_mp4toannexb \
 --enable-demuxer=mov --enable-demuxer=m4v --enable-demuxer=mpegts --enable-demuxer=flv\
 --enable-demuxer=rtsp --enable-demuxer=rtp --enable-demuxer=hls \
 --enable-muxer=mp4 --enable-muxer=flv --enable-muxer=mpegts \
 --enable-protocol=file --enable-protocol=rtmp --enable-protocol=rtmpt --enable-protocol=tcp \
 --enable-protocol=rtp --enable-protocol=udp --enable-protocol=hls\
 --enable-decoder=h264 --enable-decoder=aac --enable-decoder=pcm_alaw --enable-decoder=pcm_mulaw --enable-decoder=mp3 \
 --enable-network \
 --enable-pthreads \
 --enable-ffmpeg --enable-ffplay --enable-ffprobe \



make -j8

rm -rf compile/*
cp libavformat/libavformat.*.dylib compile/
cp libavutil/libavutil.*.dylib compile/
cp libavcodec/libavcodec.*.dylib compile/
cp libswscale/libswscale.*.dylib compile/
cp libswresample/libswresample.*.dylib compile/

rm -rf ../Frameworks/*
cp libpostproc/libpostproc.*.dylib ../Frameworks/
cp libavresample/libavresample.*.dylib ../Frameworks/
cp libavfilter/libavfilter.*.dylib ../Frameworks/
cp libavdevice/libavdevice.*.dylib ../Frameworks/
cp libavformat/libavformat.*.dylib ../Frameworks/
cp libavutil/libavutil.*.dylib ../Frameworks/
cp libavcodec/libavcodec.*.dylib ../Frameworks/
cp libswscale/libswscale.*.dylib ../Frameworks/
cp libswresample/libswresample.*.dylib ../Frameworks/

