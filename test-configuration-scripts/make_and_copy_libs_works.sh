#!/bin/sh

make clean
./configure --shlibdir=@executable_path/../Frameworks --enable-swscale --enable-avfilter --enable-avresample --enable-libmp3lame --enable-libvorbis --enable-libopus --enable-libtheora --enable-libschroedinger --enable-libopenjpeg --enable-libmodplug --enable-libvpx --enable-libspeex --enable-libass --enable-libbluray --enable-gnutls --enable-fontconfig --enable-libfreetype --disable-indev=jack --disable-outdev=xv --mandir=/opt/local/share/man --enable-shared --enable-pthreads --cc=/usr/bin/clang --enable-vda --arch=x86_64 --enable-yasm --enable-gpl --enable-postproc --enable-libx264 --enable-libxvid 

make -j8

rm -rf compile/*
cp libavformat/libavformat.*.dylib compile/
cp libavutil/libavutil.*.dylib compile/
cp libavcodec/libavcodec.*.dylib compile/
cp libswscale/libswscale.*.dylib compile/
cp libswresample/libswresample.*.dylib compile/

rm -rf ../Frameworks/*
cp libpostproc/libpostproc.*.dylib ../Frameworks/
cp libavresample/libavresample.*.dylib ../Frameworks/
cp libavfilter/libavfilter.*.dylib ../Frameworks/
cp libavdevice/libavdevice.*.dylib ../Frameworks/
cp libavformat/libavformat.*.dylib ../Frameworks/
cp libavutil/libavutil.*.dylib ../Frameworks/
cp libavcodec/libavcodec.*.dylib ../Frameworks/
cp libswscale/libswscale.*.dylib ../Frameworks/
cp libswresample/libswresample.*.dylib ../Frameworks/
