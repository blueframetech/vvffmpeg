FROM ubuntu:18.04
ARG CFLAGS="-I/tmp/compile/win32/include"
ARG LDFLAGS="-L/tmp/compile/win32/lib"
ARG PKG_CONFIG_PATH="/tmp/compile/win32/lib/pkgconfig"

WORKDIR "/opt"

RUN apt-get update && apt-get install -y build-essential libssl-dev libmp3lame-dev pkg-config nasm yasm curl gcc-mingw-w64 zip unzip git

RUN cd /tmp && git clone https://bitbucket.org/blueframetech/vvffmpeg.git

RUN cd /tmp/vvffmpeg && git checkout master && git pull origin && git log --name-status HEAD^..HEAD && \
./configure \
 --enable-memalign-hack \
 --arch=x86_64 \
 --target-os=mingw32 \
 --cross-prefix=x86_64-w64-mingw32- \
 --pkg-config=pkg-config \
 --extra-cflags=${CFLAGS} \
 --extra-ldflags=${LDFLAGS} \
 --prefix="/tmp/compile/win32" \
 --enable-w32threads \
 --enable-shared \
 --disable-doc \
 --disable-decoders \
 --disable-encoders \
 --disable-devices \
 --enable-avutil \
 --enable-avformat \
 --enable-avcodec \
 --enable-swscale \
 --enable-swresample \
 --enable-encoder=aac \
 --enable-bsf=h264_mp4toannexb \
 --enable-demuxer=mov \
 --enable-demuxer=m4v \
 --enable-demuxer=mpegts \
 --enable-demuxer=flv\
 --enable-demuxer=rtsp \
 --enable-demuxer=rtp \
 --enable-demuxer=hls \
 --enable-muxer=mp4 \
 --enable-muxer=flv \
 --enable-muxer=mpegts \
 --enable-protocol=file \
 --enable-protocol=rtmp \
 --enable-protocol=rtmpt \
 --enable-protocol=rtmps \
 --enable-protocol=tcp \
 --enable-protocol=rtp \
 --enable-protocol=udp \
 --enable-protocol=hls \
 --enable-decoder=h264 \
 --enable-decoder=aac \
 --enable-decoder=pcm_alaw \
 --enable-decoder=pcm_mulaw \
 --enable-decoder=mp3 \
 --enable-network \
 --enable-parsers \
 --enable-ffmpeg && \
 make && make install && make distclean

RUN cd /tmp/compile/win32/bin/ && zip output.zip *.dll ffmpeg* && cp output.zip /opt/

