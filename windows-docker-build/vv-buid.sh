#!/bin/bash

mkdir -p vvdist

docker build -t compile-ffmpeg:latest -f vvffmpeg-Dockerfile .
docker run --rm --mount type=bind,src=${PWD}/vvdist,dst=/vvdist compile-ffmpeg:latest cp output.zip /vvdist
