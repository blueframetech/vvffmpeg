#!/bin/bash

mkdir -p dist

docker build -t compile-static-ffmpeg:latest -f ffmpeg-Dockerfile .
docker run --rm --mount type=bind,src=${PWD}/dist,dst=/dist compile-static-ffmpeg:latest cp output.zip /dist
