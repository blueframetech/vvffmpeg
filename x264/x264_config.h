#define X264_BIT_DEPTH     8
#define X264_GPL           1
#define X264_INTERLACED    1
#define X264_CHROMA_FORMAT 0
#define X264_REV 5
#define X264_REV_DIFF 1
#define X264_VERSION " r5+1M 864f45e"
#define X264_POINTVER "0.135.5+1M 864f45e"
