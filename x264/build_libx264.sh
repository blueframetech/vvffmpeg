#/bin/bash

#This build script assumes you are trying to cross compile win32 libs from Ubuntu
#with the ffmpeg.zeranoe.com MingGW-w64 build script installed.  To install: 
#http://ffmpeg.zeranoe.com/blog/?p=187

make clean
WD=`pwd`

if [[ "$1" == "" ]]
then
  echo "Usage: ./build_libx264 [ABSOLUTE PATH TO VVFFMPEG LIB DIR]"

else
echo "VVFFmpeg at:" $1

if [[ "$OSTYPE" == "darwin12" ]]
then
  echo "Building Libx264"
  sleep 2

  ./configure --host=i686-w64-mingw32 --cross-prefix=i686-w64-mingw32- --prefix=$1 --enable-shared --enable-static
  make install; make install-lib-dev; make install-lib-static;
  
else
  echo $OSTYPE" isn't correct, expects OSX...exiting"
  sleep 2
fi
fi
