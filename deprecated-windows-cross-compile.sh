#!/bin/bash

# This build script should allow you to cross compile Win32 Libs with OS X
# OS X requires ffmpeg.zeranoe.com MingGW-w64 build script installed.  To install: 
# http://ffmpeg.zeranoe.com/blog/?p=187
# If you're having trouble building, the i686-w64-mingw32/bin directory must be added
# to your PATH.
# This script no longer gives the user the option to build libx264.  This was 
# removed from the ProductionTruck project due to licensing issues.

#while true; do
#    read -p "Would you like to rebuild x264?  y = yes, n = no  " yn
#    case $yn in
#       [Yy]* ) cd x264; echo "building Libx264"; ./build_libx264.sh ../compile/win32/; cd ..; break;;
#        [Nn]* ) echo "Not compiling x264"; sleep 2; break;;
#    esac
#done

echo "NOT BUILDING LIBX264, uncomment while loop in build script to build"

WD=`pwd`

echo "Cross compiling FFmpeg libraries for WIN32"
sleep 2

make clean

CFLAGS="-I$WD/compile/win32/include"
LDFLAGS="-L$WD/compile/win32/lib"
PKG_CONFIG_PATH="$WD/compile/win32/lib/pkgconfig"

echo "Running ./configure script"

./configure --enable-memalign-hack --arch=x86 --target-os=mingw32 --cross-prefix=i686-w64-mingw32- \
--pkg-config=pkg-config \
--extra-cflags=${CFLAGS} --extra-ldflags=${LDFLAGS} --prefix="$WD/compile/win32" \
--disable-all --disable-everything --enable-w32threads --disable-zlib \
--enable-shared \
--enable-bsf=h264_mp4toannexb \
--enable-demuxer=mov --enable-demuxer=flv \
--enable-muxer=mpegts --enable-muxer=flv --enable-muxer=mp4 \
--enable-encoder=aac --enable-encoder=mpeg4 \
--enable-decoder=mp3 --enable-decoder=h264 --enable-decoder=aac \
--enable-protocol=file --enable-protocol=rtmp --enable-protocol=rtmpt \
--enable-avformat --enable-avutil --enable-avcodec --enable-swscale --enable-swresample

echo "Running make"
sleep 2
make -j8 
make install  

echo "build complete"
sleep 2

cd $WD/compile/win32/bin

zip ffmpeg_includes.zip *.dll 

echo "Build complete, your libs are in compile/win32/bin/ffmpeg_includes.zip"

